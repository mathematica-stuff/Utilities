# utilities

Contains some very useful extensions of everyday Mathematica workflow.
Mostly dedicated to robustness and safety, such as

* possibility to provide type-checked definitions, both if input and return types
* replace which aborts if none of replacement rules could not be applied 
* a user-friendly replacement of nested With construction  

Bases on the private code of Ben Page and some other ideas and code snippets found in Internet.

Usage
------

Impport as any normal Mathematica package.

Even better, the latest version of the package can be used directly from the repository like this:
```Mathematica
Needs["Utilities`", "https://gitlab.com/mathematica-stuff/utilities/raw/master/Utilities.m"];
```
No downloading or installation is required in this case.
