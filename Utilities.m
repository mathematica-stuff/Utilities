BeginPackage["Utilities`"];

Unprotect["Utilities`*"];


SetFailureHandle::usage="SetFailureHandle[method]
determines what is done in case erros are encountered in checks performed by addTerminationPattern, ScopeEnclose, etc.
\
If method is Abort, then Abort[] is called (the default for backwards compatibility).
If method is Exit, then Exit[1] is called.
If method is UtilitiesException, an exception is thrown.
";

UtilitiesException::usage="An exception that can be thrown in case of failure when SetFailureHandle[\"Exception\"] was called";

addTerminationPattern::usage = "
    Add a definition to a function which terminates upon being invoked with unknown input.
";

requirePrecondition::usage = "
    Automatically add a termination pattern to everything wrapped inside this function.
    NOTE: is is absolutely crucial to separate each statement with ";" inside this environment!
";

failingReplace::usage= "
    Like Replace, but aborts if the none of the rules match.
    Syntax: failingReplace[rules][expr]
";

Let::usage = "
    Similar to System`With, but allows to access the results of all previously assigned values.
    Let[_,_,_,...] returns the result of the last statement.

    This is equivalent to nested usage or System`With for each assignment. Or to an undocumented feature of System`With.

    Implementation of is taken from https://mathematica.stackexchange.com/a/64944
";

Scope::usage = "\
Scope[{globals..}, expressions]
    Works similar to Block, but the role of global vs local symbols is reversed. Returns the result of the last expression.

    All symbols encountered in expressions are automatically local (using Block internally).
    To explicitly lift a symbol to the enclosing scope it should be mentioned in globals in the same way how local symbols are given in Block.

    Note that if some symbol had a global variable and is NOT listed in globals, its value will be used in expressions,
    but its modifications will not be translated to the outer scope.

    The idea is taken from https://mathematica.stackexchange.com/a/31720


Scope[expressions]
    is equivalent to Scope[{}, expressoins]
";


ScopeEnclose::usage = "Same as Scope, but additionally wraps Enclose around it which, if any confirmation fails within it, prints the latter and 
does what is determined by SetFailureHandle.
";

showTiming::usage = "
    Echos the time taken for the computation. Returns just the results compared to the return of System`Timing[].
    The function which does the printing can be specified through a value of the option \"Print\", default is System`Print.
";


SymbolNamesToIndices::usage = "SymbolNamesToIndices[heads : {_Symbol...}][expr_]
    converts all symbols in expr, that can be interpreted as any of symbols given in heads with indices being part of their original name,
    to the form where indices are explicit arguments.
    Assumes that each index is given exactly by one digit.

    The matching of symbols in the expressoin is done disregarding the contexts of both provided heads and symbols in the expression.
    However the context of heads is preserved in the resulting exression.

    For example, 
        SymbolNamesToIndices[s12 + s23,{s}]
    produces 
        s[1,2] + s[2,3].
";

IndicesToSymbolNames::usage ="IndicesToSymbolNames[heads : {_Symbol...}][expr_]
    is the inverse of IndicesToSymbolNames
";

getInputTypes::usage = "Returns a list of the types associated to a function";


typedEnv::usage = "
    typedEnv forces all statements inside it to return an expression of specified TYPE.
    This implements partial strong typing.
    NOTE: it is absolutely crucial to separate each statement with ";" inside this environment!
";

TYPE::usage= "
    Use to set the type of the next line (in a compound expression) whilst in a typedEnv.
    Can be any pattern.
";
ReturnTyped::usage= "Returns from the current function, asserting the type structure";


$DEBUG::usage = "Global variable for storing debug variables.";

PRINT::usage = "PRINT[expr] prints unevaluated form of expr before printing expr.";

DumpSymbol::usage = "Dumps a symbol to a file with the same name as the symbol's (in the current directory).";
GetSymbol::usage = "Loads a symbol from the file with the same as the symbol.";

Collect2::usage = "Collect2[expr_, varsPattern_]
Represents `expr` as a polynomial, which variables are taken to be all (smallest) subexpressions in `expr` mathching any of patterns from `varsPattern`.
Equivalently, collects together all terms which are free of any occurrence of subexpressions matching any of `varsPattern`.
`varsPattern` can be a list of patterns.

Has 2 options:
* \"Simplify\":
    function to apply to each coefficient
    default: Simplify
* \"Parallelize\":
    whether to apply function to coefficient in parallel
    default: False 
";


fString::usage="fString[str_String]
Consumes all substrings surrounded by \"``\" in string `str` and replaces them by their value as Mathematica InputForm expressions.

For example,

Block[{x=10}, fString[\"I am a ``x^2``\"]] will return \"I am a 100\".

Contexts inside `` are correctly interpreted.
";

Series0::usage = "Series0[expr_,var_,order_] or Series0[var_,order_][expr_]
is the same as Series[expr,{var,0,order}].
order can be ommitted, in which case it's assumed to be 0.
";

MapOverSeries::usage = "MapOverSeries[f_][series_]
Maps f over Series series.
In case of nested serieses (in multiple variables) maps over each element of the deepest series.
";

MapIndexedFlat::usage = "MapIndexedFlat
is like System`MapIndexed applied on level {1}, but the mapped function receives in its second argument the index,
instead of the index in a list like in the system one.
";

MapProgress::usage = "MapProgress[f][onto]
the same as System`Map, but prints information about the progress.
";


PrintIndexed::usage = "PrintIndexed[expr_]
prints each element of expression expr (at level 1) prefixed by its index.
";

PositionFlat::usage = "PositionFlat[where,what]
determines at which position 'what' can be found in 'where' at level {1}.

It is equivalent to the flattened result of System`Position applied on level {1} with Heads->False.

Position[what] is an operator form.
";

UniqueCases::usage = "UniqueCases[expr, pattern, level]
finds unique occurrences of pattern in expression.
This is a simple wrapper around Cases, so it accepts the same format.

Default levels is {0,Infinity}

UniqueCases[pattern]
functional form is also provided.
";

getLinearCoefficients::usage = "getLinearCoefficients[vars][expr]
given a linear combination expr of vars, return a list of their coefficients.
Aborts if expr is not linear in vars. 
Emits a message an inhomogeneous term is found.

getLinearCoefficients[pattern][expr]
same but will construct a list of variables from anything that matches the given pattern.
Return {vars, coefficients}.
";


ListReshape::usage = "ListReshape[flat_List, structure_]
reshapes (or unflattens) a flat list into a given structure.
The structure is given in the format where the inner most leaves are how many elements to take from the flat list.
For example, {2, {3}, {{{3}}}}, will distribute 8 elements like this:
{x, x, {x,x,x}, {{{x,x,x}}}}
";


ImportChecked::usage = "ImportChecked[filename]
uses import to import a file.
If it fails, attempts again number of attemps given by option \"NAttempts\" with a delay given by option \"Delay\".
Returns $Failed if all attempts fail.
";

ThrowFailed::usage = "ThrowFailed
Converts a $Failed expression into an exception. 
This is useful to propogate the failure from parallel kernels.
";


ClearInOut::usage = "ClearInOut[]
releases memory occupied by storing In an Out of each priou evaluation.
";

ParallelTry2::usage = "ParallelTry2[f_, args_] 
is same as System`ParallelTry, but custom implementation to control internal behavior beter.
";



Begin["`Private`"];

$FailureHandle = Abort;

SetFailureHandle::unknown = "Unknown error handling method `1`";

SetFailureHandle[method_] := Switch[method,
    Abort, $FailureHandle = Abort,
    Exit, $FailureHandle = Exit,
    UtilitiesException, $FailureHandle = UtilitiesException,
    _, 
    Message[SetFailureHandle::unknown, method];
    handleFailure[]; 
];

addTerminationPattern[SetFailureHandle];

handleFailure[] := Switch[$FailureHandle,
    Abort, Abort[],
    Exit, Print["Exit[1]"]; Exit[1],
    UtilitiesException, Throw[UtilitiesException]
];


SetAttributes[Let, HoldAll];

SyntaxInformation[Let] = {
    "LocalVariables"->{"Solve",{1,Infinity}}
};

SetAttributes[partThread, HoldAll];
partThread[l___, rhs_] :=
  Join @@ Replace[
    MapIndexed[Append[#, First@#2] &, Thread[Hold[{l}]]],
    Hold[s_, i_] :> Hold[s = rhs[[i]]], {1}
];


SetAttributes[{Let, let}, HoldAll];

Let /: Verbatim[SetDelayed][lhs_, rhs : HoldPattern[Let[__, _]]] := 
   Block[{With}, Attributes[With] = {HoldAll};
      lhs := Evaluate[rhs /. HoldPattern[With[{}, b_]] :> b]
   ];

Let[args___, body_] := let[{args}, body, {}, {}];

let[{}, body_, {}, _] := With[{}, body];

let[{Set[{s___}, rhs_], rest___}, body_, dec_, syms_] :=
   Module[{temp},
     partThread[s, temp] /. Hold[d___] :>
        let[{temp = rhs, d, rest}, body, dec, syms]
   ];

let[
   {Set[sym_, rhs_], rest___}, 
   body_, 
   {decs___}, 
   {syms___}
] /; FreeQ[Unevaluated[rhs], Alternatives[syms]] :=
     let[{rest}, body, {decs, sym = rhs}, {syms, HoldPattern[sym]}];

let[{args___}, body_, {decs__}, _] :=
   Block[{With},
     Attributes[With] = {HoldAll};
     With[{decs},Evaluate[let[{args}, body, {}, {}]]]
];

SetAttributes[showTiming,HoldAll];

Options[showTiming] =  {"Print" -> System`Print};

showTiming[x_,mess___, OptionsPattern[]] := (
    If[Length[{mess}]>0, OptionValue["Print"][mess]; ];
    With[{res = AbsoluteTiming[x]}, 
        OptionValue["Print"]["Took ",res[[1]], "s"];
        res[[2]]
    ]
);


(* TODO: Somewhere in here, evaluation of f[args] gets leaked. *)

addTerminationPattern[head_] := head[args___] := (
    Print["ERROR: invocation of \n",HoldForm@head[args]];
    Print["didn't match any of definitions"];
    (* The definition for termination pattern is added at the end. It is useless to show it here, so remove *)
    Print[Most[getInputTypes[head]]];
    Print["Aborting!"];
    Print["Stack: ", Stack[]];
    $DEBUG["TYPEFAILURE"]=Hold[head[args];$DEBUG["TYPEFAILURESTACK"]=Stack[];];
    handleFailure[]
);


SetAttributes[requirePrecondition,HoldAll];

requirePrecondition[CompoundExpression[expr_,exprs__]]:=CompoundExpression[requirePrecondition[expr],requirePrecondition[CompoundExpression[exprs]]];

requirePrecondition[def:SetDelayed[(Verbatim@Condition)[f_Symbol[___],_],_]]:=(addTerminationPattern[f];def);

requirePrecondition[def:SetDelayed[f_Symbol[___],_]]:=(addTerminationPattern[f];def);

requirePrecondition[x_]:=(x);

getInputTypes[x_Symbol]:=(First/@DownValues[x])/.HoldPattern->HoldForm;


failingReplace[{}][expr_]:=expr;
failingReplace[rules_List][expr_]:=failingReplace[rules,{}][expr];
failingReplace[{a_->b_,rules___},_][expr_]/;MatchQ[expr,a]:=Replace[a->b][expr];
failingReplace[{a_:>b_,rules___},_][expr_]/;MatchQ[expr,a]:=Replace[a:>b][expr];
failingReplace[{rule_,rules___},{checked___}][expr_]:=failingReplace[{rules},{checked,rule}][expr];
failingReplace[{},checked_][expr_] := (
    Print["ERROR: no rules \n", $DEBUG["BADRULES"] = checked ,"\n matched expression \n", $DEBUG["BADEXPR"] = expr];
    Print["Aborting!"];
    Print["Stack: ", Stack[]];
    handleFailure[]
);


(* "Strong" typing framework *)

ReturnTyped[result_, type_, ___] /; MatchQ[result, type] := Return[Return[result]];
ReturnTyped[result_, type_] := (
    Print["ERROR: type '", type,"' not matched by:"];
    Print[$DEBUG["TYPEFAILURE"]=result];
    Print["Aborting!"];
    Print["Stack: ", Stack[]];
    handleFailure[]
);
ReturnTyped[result_, type_, f_HoldForm] := (
    Print["ERROR: result of invoking '", f, "':"];
    Print[$DEBUG["TYPEFAILURE"]=result]; 
    Print["doesn't match type '", type,"'"];
    Print["Aborting!"];
    Print["Stack: ", Stack[]];
    handleFailure[]
);


(* Property checking*)
ClearAll[hasTypedReturn];
SetAttributes[hasTypedReturn,HoldFirst];
hasTypedReturn[code_]:=!FreeQ[Hold@code,Unevaluated@ReturnTyped[_,_]];

ClearAll[noTypedReturn];
SetAttributes[noTypedReturn,HoldFirst];
noTypedReturn[code_]:=FreeQ[Hold@code,Unevaluated@ReturnTyped[_,_]];

ClearAll[hasReturn];
SetAttributes[hasReturn,HoldFirst];
hasReturn[code_]:=!FreeQ[Hold@code,Unevaluated@Return[_]];

ClearAll[noReturn];
SetAttributes[noReturn,HoldFirst];
noReturn[code_]:=FreeQ[Hold@code,Unevaluated@Return[_]];

(*Type checking*)

ClearAll[typedEnv];
SetAttributes[typedEnv,HoldAll];

(* Recurse over expressions, ignoring Null *)
typedEnv[xs__;Null]:=typedEnv[CompoundExpression[xs]];
typedEnv[x_;xs__]:=(typedEnv[x];typedEnv[CompoundExpression[xs]]);
typedEnv[CompoundExpression[x_]]:=typedEnv[x];

(* Type annotation code: *)

typedEnv[CompoundExpression[TYPE[type_],HoldPattern@SetDelayed[f_,body_?noReturn],rest___]]:=
(typedEnv[CompoundExpression[TYPE[type],f:=Return[body],rest]]);

typedEnv[CompoundExpression[TYPE[type_],typedLine_?hasReturn,rest___]]:=
(Hold[typedLine] /. HoldPattern@Return[value_] :> Unevaluated[ReturnTyped[value,type]] /. Hold[x_] :> typedEnv[CompoundExpression[x,rest]]);


(* Typed function defining code *)
typedEnv[code:HoldPattern@SetDelayed[function_[___],_?hasTypedReturn]]:=(Hold[code]/.HoldPattern@ReturnTyped[value_,type_]:>Unevaluated[ReturnTyped[value,type,HoldForm@function]]//ReleaseHold);

typedEnv[code:HoldPattern@SetDelayed[function_,body_?noTypedReturn]]:= (
    Print["ERROR: function definition '",HoldForm[function],"' lacks return type:"];
    Print[HoldForm[body]];
    handleFailure[]
);

typedEnv[code_]:=code;

SetAttributes[PRINT, HoldAll];
PRINT[expr_] := Print[Unevaluated[expr], " = ", expr];

Options[DumpSymbol] = {"Prefix"->""};
Options[GetSymbol] = {"Prefix"->""};

SetAttributes[DumpSymbol, HoldAll];
SetAttributes[GetSymbol, HoldAll];

DumpSymbol[s_Symbol, opts:OptionsPattern[]] := Let[
    sname = SymbolName[Unevaluated[s]], 
    filename = OptionValue["Prefix"]<>sname <>".mx",
    If[FileExistsQ[filename], DeleteFile[filename]];
    DumpSave[filename, sname];
];

GetSymbol::nofile = "Cannot get symbol `1` from file, file doesn't exist!";
GetSymbol[s_Symbol, opts:OptionsPattern[]] := Let[
    sname = SymbolName[Unevaluated[s]], 
    filename = OptionValue["Prefix"]<>sname <>".mx",
    If[ FileExistsQ[filename], 
        Get[filename];
        Return[True],
        Message[GetSymbol::nofile, filename];
        Return[False];
    ];
];

GetSymbol[s_Symbol, code_, opts:OptionsPattern[]] := Scope[{s},
    sname = SymbolName[Unevaluated[s]];
    filename = OptionValue["Prefix"]<>sname <>".mx";
    If[ FileExistsQ[filename], 
        Get[filename];
        Return[True],
        Message[GetSymbol::nofile, filename];
        code;
        DumpSave[filename, sname];
    ];
]


Options[Collect2] = {"Parallelize" -> False, "Simplify" -> Simplify, Modulus -> 0};

SyntaxInformation[Collect2] = {"ArgumentsPattern" -> {_,_, OptionsPattern[]}};

Collect2[expr_, varsPattern_, OptionsPattern[]] := OptionValue["Simplify"][expr] /; FreeQ[expr, 
    If[Head[varsPattern]===List, Alternatives@@varsPattern, varsPattern]
];

Collect2[expr_, varsPattern_List, opts:OptionsPattern[]] := Let[
   f = OptionValue["Simplify"],
   allvars = Variables[expr],
   (* Mathematica does not treat roots as independent variables, so we add them manually *)
   roots = DeleteDuplicates[Flatten[Cases[expr,Power[#,_Rational],Infinity]& /@ varsPattern]],
   vars = Sort @ Join[Select[allvars, Function[x, AnyTrue[varsPattern, ! FreeQ[x, #] &]]], roots],
   coeffs = CoefficientRules[expr, vars, FilterRules[{opts}, Options[CoefficientRules]]],
   transf = If[TrueQ[OptionValue["Parallelize"]], 
        ParallelMap[ (#[[1]] -> f[#[[2]]]) &, coeffs],
        MapAt[f, coeffs, {All, 2}]
   ],
   FromCoefficientRules[transf, vars]
];

Collect2[expr_, varsPattern_, opt:OptionsPattern[]] := Collect2[expr, {varsPattern}, opt];

ClearAll[Series0];
Series0[var_,order_Integer][expr_] := Series[expr,{var,0,order}];
Series0[var_][expr_] := Series[expr,{var,0,0}];
Series0[expr_,var_,order_Integer] := Series[expr,{var,0,order}];

SetAttributes[mapOverSeriesHelper,HoldAll];

mapOverSeriesHelper[f_][ll_SeriesData] := MapOverSeries[f][ll];
mapOverSeriesHelper[f_][ll_] := f[ll];


MapOverSeries[f_][expr_SeriesData] := ReplacePart[expr, 3-> Map[mapOverSeriesHelper[f],expr[[3]]]];



addTerminationPattern[Collect2];

SymbolNamesToIndices::notsymbols = "`1` is not a list of symbols!";

SymbolNamesToIndices[heads : {_Symbol...}][expr_] := expr /. 
    HoldPattern[ g_Symbol /; StringMatchQ[ SymbolName[g], (Alternatives @@ (SymbolName/@heads)) ~~ DigitCharacter ..]] :> 
        Let[
             chars = SymbolName@g,
             sname = First@StringCases[chars, Longest[LetterCharacter ..]],
             indices = First@StringCases[chars, Longest[DigitCharacter ..]],
             SelectFirst[heads,SymbolName[#]==sname &][ Sequence @@ ToExpression /@ (Characters[indices])]
         ];


IndicesToSymbolNames[heads : {_Symbol...}][expr_] := expr /. 
    (h : (Alternatives @@ heads))[ inds : (_Integer?Positive ..)] :> 
        Symbol[SymbolName[h] <> StringRiffle[ToString /@ {inds}, ""]];

SymbolNamesToIndices[l_List][_] := (Message[SymbolNamesToIndices::notsymbols, l]; handleFailure[]);
IndicesToSymbolNames[l_List][_] := (Message[SymbolNamesToIndices::notsymbols, l]; handleFailure[]);

SetAttributes[MapIndexedFlat,HoldAll];

MapIndexedFlat[f_,expr_] := MapIndexed[f[#1,First[#2]]&,expr];
MapIndexedFlat[f_][expr_] := MapIndexed[f[#1,First[#2]]&,expr];


PrintIndexed[expr_] := (expr // MapIndexedFlat[Print[#2, "\t", #1]&]; Null);


ClearAll[MapProgress];

SetAttributes[MapProgess, HoldAll];

Options[MapProgress] := {"Delay" -> 5};

MapProgress[f_,onto_] := MapProgress[f][onto];

MapProgress[f_][onto_] := Scope[
    totaltime = 0;
    len = Length[onto];
    
    flstr[x_] := NumberForm[x,{4,3}] // ToString;

    str = "waiting for first evaluation to finish";

    Monitor[
        MapIndexedFlat[
            (
                {time, val} = AbsoluteTiming[f[#1]];
                totaltime += time;
                str = StringTemplate["`1` from `2`,     took `3`s, total time `4`s"][#2, len, flstr@time, flstr@totaltime];
                val
            )&, 
            onto
        ], 
        str, 
        OptionValue[MapProgress, "Delay"]
    ]
];



SetAttributes[PositionFlat,HoldAll];
PositionFlat[where_, what_] := Position[where, what, {1}, Heads -> False] // Flatten;
PositionFlat[what_][where_] := PositionFlat[where, what];


ClearAll[fString];
Options[fString] = {"Format" -> InputForm};

fString[str_String, opts:OptionsPattern[]]:= Enclose[ConfirmMatch[
    StringReplace[str, Shortest["``"~~(expr___)~~"``"]:> StringReplace[ToString[ToExpression[expr],OptionValue["Format"]], "\""->""]],
    _String?(Not@*StringContainsQ["``"]),
    "Unmatched ``"
]];

addTerminationPattern[fString];



ClearAll[flatHold];
SetAttributes[flatHold, {Flat, HoldAll}];
{a__flatHold} ^:= flatHold[a];

(scope : Block | Module)[flatHold[a___], body_] ^:= scope[{a}, body];


ClearAll[getSymbols, getSymbolsList, getLevel1Symbol];
SetAttributes[getSymbols, HoldAllComplete];
SetAttributes[getSymbolsList, HoldAllComplete];

getSymbolsList[l_List] := Map[getSymbolsList, Unevaluated[l]];
getSymbolsList[s_Symbol] := flatHold[s];

SetAttributes[getLevel1Symbol, HoldAllComplete];

getLevel1Symbol[] = {flatHold[]};
getLevel1Symbol[] = {flatHold[]};
getLevel1Symbol[s_Symbol,rest___] := {flatHold[s], getLevel1Symbol[rest]};
getLevel1Symbol[(s_)[___], rest___] := {flatHold[s], getLevel1Symbol[rest]};

getSymbols[Set[List[ss:(_Symbol..)],_]] := flatHold[ss];
getSymbols[Set[l_List, _]] := Map[getSymbolsList, Unevaluated[l]];
getSymbols[(UpSet | UpSetDelayed)[_[ss__],___]] := getLevel1Symbol[ss];
getSymbols[(Set | SetDelayed | TagSet | TagSetDelayed)[ a_Symbol, __]] := flatHold[a];
getSymbols[(Set | SetDelayed)[a_, _]] := Cases[Unevaluated@a, r_Symbol /; !  MemberQ[Attributes[r], Protected | Locked | ReadProtected] :> flatHold[r], {0, Infinity}, 1, Heads -> True];
getSymbols[(Clear | ClearAll)[ss__Symbol]] := flatHold[ss];
getSymbols[(Clear | ClearAll)[___]] := flatHold[];
getSymbols[expr_] := Cases[Unevaluated@expr, s : _Set | _SetDelayed | _TagSet | _TagSetDelayed | _UpSet | _UpSetDelayed | _Clear | _ClearAll :> getSymbols[s], Infinity];

ClearAll[Scope];
SetAttributes[Scope, HoldAll];

Scope[{globals : (_Symbol | _Set | _SetDelayed) ...}, body_] := With[
   {toskip = Cases[flatHold[globals], s_Symbol :> flatHold[s]]},
   
 (* Put globals into Global` or equivalent *)
   globals;

   With[{vars = Complement[getSymbols[body] // DeleteDuplicates, flatHold[toskip]]}, Block[vars, body]]
];

Scope[body_] := With[{vars = getSymbols[body] // DeleteDuplicates}, Block[vars, body]];


ClearAll[ScopeEnclose];
SetAttributes[ScopeEnclose, HoldAll];

ScopeEnclose[args__] := Enclose[Scope[args], Function[m,
    If[$FrontEnd =!= Null,
        Print[m];
        ,
        Print["ScopeEnclose ERROR: ",m//First]; 
        Print[m["MessageParameters"]]; 
        Print["Stack: ", Stack[]];
    ];
    handleFailure[];
]];


ClearAll[UniqueCases];
UniqueCases[expr_, pattern_, levelspec_:{0,Infinity}, rest___] := Cases[expr, pattern, levelspec, rest]//DeleteDuplicates//Sort;
UniqueCases[pattern_][expr_] := UniqueCases[expr, pattern];



Clear[getLinearCoefficients];

getLinearCoefficients::inhm = "Inhomogeneous term 
`1`
encountered";

getLinearCoefficients::novars = "No variables mathcing pattern `1`";

getLinearCoefficients::nonlin = "Encountered an expression that is not linear in variables `1`, array dimensions are `2`";

Options[getLinearCoefficients] = {"SparseOutput" -> False};


getLinearCoefficients[vars_List, opts:OptionsPattern[]][expr_] := Scope[
    If[expr =!= 0, 
        Message[getLinearCoefficients::inhm, expr];
    ];

    Table[0, Length[vars]]
] /; FreeQ[expr, Alternatives@@vars];

getLinearCoefficients[pattern_, opts:OptionsPattern[]][expr_] := Scope[
    vars = UniqueCases[expr, pattern];
    If[Length[vars] == 0, Message[getLinearCoefficients::novars,pattern]];
    {vars, getLinearCoefficients[vars, opts][expr]}
];


getLinearCoefficients[vars_List, opts:OptionsPattern[]][expr_] := Scope[
    nvars = Length[vars];
    coeffs = CoefficientArrays[expr, vars];

    Which[ 
        Length[coeffs] == 1 , If[expr=!=0, Message[getLinearCoefficients::inhm, expr]]; Return[{}], 
        Length[coeffs]>2 || Dimensions[coeffs[[2]]]!={nvars}, Message[getLinearCoefficients::nonlin, vars, Dimensions /@ coeffs]; handleFailure[]
    ];

    If[First[coeffs] =!= 0,
        Message[getLinearCoefficients::inhm, First[coeffs]];
    ];
        

    coeffs = coeffs[[2]];

    If[TrueQ[OptionValue["SparseOutput"]],
        coeffs,
        Normal@coeffs
    ]
];

getLinearCoefficients[expr_, vars_List, opts:OptionsPattern[]] := getLinearCoefficients[vars, opts][expr];

(*addTerminationPattern[getLinearCoefficients];*)

ClearAll[ListReshape];


ListReshape[flat_List, structure_] := ScopeEnclose[
	total = 0;
	Map[(total+=ConfirmMatch[#, _Integer?Positive])&, structure, {-1}];
	ConfirmAssert[Length[flat]==total];
	
	current = flat;

	Map[({take, current} = TakeDrop[current, #]; Sequence@@take)&, structure, {-1}] 

];



Clear[ImportChecked];

Options[ImportChecked] = {"NAttempts" -> 3, "Delay"-> 2, "Format" -> Automatic};

ImportChecked::error = "Encountered a problem while importing file `1` after `2` attempts";

ImportChecked[filename_String, opts:OptionsPattern[]] := Block[{result = $Failed, nattempt = 0},
	Do[	

                If[OptionValue["Format"] === Automatic,
                    result = Import[filename];
                    ,
                    result = Import[filename, OptionValue["Format"]];
                ];

		If[result =!= $Failed, Return[result, Block]];
		
		++nattempt;
		Pause[OptionValue["Delay"]];
		,
		OptionValue["NAttempts"]
	];
	Message[ImportChecked::error, filename,  nattempt];
	result
];

Clear[ThrowFailed];

ThrowFailed[$Failed] := Throw[$Failed];
ThrowFailed[x_]:=x;


ClearInOut[] := (
    Unprotect[System`In, System`Out, System`InString, System`MessageList];
    Clear[System`In, System`Out, System`InString, System`MessageList];
    Protect[System`In, System`Out, System`InString, System`MessageList];
    System`$Line = 0;
);

Clear[ParallelTry2];

ParallelTry2::failed = "ParallelTry2 failed for all `1` arguments";

Options[ParallelTry2] = {"FailPattern" -> $Failed};

ParallelTry2[f_, args_List, opts:OptionsPattern[]] := Scope[

	jobs = Table[ParallelSubmit[{x}, Catch[f[x]]],{x,args}];

	res = $Failed;
        (*nid = 0;*)
		
	CheckAbort[
		While[And[MatchQ[res,OptionValue["FailPattern"]], Length[jobs]>0], 
                        (*++nid;*)
			{res, id, jobs} = WaitNext[jobs];
                        (*Echo[nid];*)
		];
		,
		AbortKernels[];
	];
	
	AbortKernels[];

        If[MatchQ[res,OptionValue["FailPattern"]], 
            Message[ParallelTry2::failed, Length[args]];
            Return[$Failed];
        ];

	res
];

addTerminationPattern[ParallelTry2];


End[];

Protect["Utilities`*"];

Unprotect[$DEBUG];


EndPackage[];
