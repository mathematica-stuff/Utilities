Needs["Utilities`"];

Begin["Utilities`"];

Unprotect[AbbreviateRoots];

AbbreviateRoots::usage = "AbbreviateRoots[expr_,symb_,OptionsPattern[]]
finds all different roots in a given expression and abbreviate them.
Symbols for abbreviations are either given by a list of symbols or by a head which will be indexed by each root.
  
An option `IgnorePattern` can be used to not abbreviate any roots matching a given pattern.";

Begin["`Private`"];

Options[AbbreviateRoots] := {"IgnorePattern" -> {}, "IgnoreNumeric" -> True};

Clear[AbbreviateRoots];

AbbreviateRoots[expr_, symb_, opts:OptionsPattern[]] := Scope[
    toignore = OptionValue["IgnorePattern"];
    toignore = {If[OptionValue["IgnoreNumeric"], _?NumericQ, Nothing], toignore}//Flatten;
    toignore = Alternatives @@ toignore;
    allroots = Cases[{expr}, x_^Rational[r_Integer, 2] /; (! MatchQ[x, toignore]) :> Factor[x]^(1/ 2), Infinity] // DeleteDuplicates // Sort;
    symbols = If[MatchQ[symb, _List] && Length[symb] >= Length[allroots], symb, Array[symb, Length[allroots]]];
    abbreviations = Association[MapIndexed[#1 -> symbols[[First@#2]] &, allroots]];
    {
        expr /.  x_^Rational[r_Integer, 2] /; (! MatchQ[x, toignore]) :>  Let[xx = Factor[x], xx^Quotient[r - Sign[r], 2] abbreviations[xx^(1/2)]^Sign[r]],
        Apply[#2 -> #1 &] /@ Normal@abbreviations 
    }
];

End[];

Protect[AbbreviateRoots];

End[];
