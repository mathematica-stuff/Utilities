BeginPackage["RingF`", {"Utilities`"}];

RingF::usage = "\
    Abstract representation of a polynomial ring in variables $RingFVariables.
    Ring operations are implemented to preserve the ring structure and to prevent
    unnecessary evaluations. 
";

$RingFVariables::usage = "\
    Current variables of RingF (global).
    Certain operations require this to be defined to work correctly.
    In particular, \"scalars\" of the ring will not be correctly identified for multiplication of polynomials.

    The reason for this to be global is that we do not want to drag it along with each RingF for efficiency.
";

toRingF::usage = "Convert expression from a normal form to a polynomial ring in variables $RingFVariables.";
fromRingF::usage = "Convert expression from polynomial ring to normal form";

MapOverRingF::usage = "Map a function over coefficients of RingF"; 
MapOverRingFParallel::usage = "Map a function over coefficients of RingF in parallel"; 

dropZeroes::usage = "Drop zero entries. They are not dropped automatically to not look at the expressions all the time.";

toMonomialList::usage = "Get an Association from RingF where keys are explicit monomials from $RingFVariables."


cancelFactor::usage = "cancelFactor[RingF, var]
attempts to cancel a variable that is a common factor
";


Begin["`Private`"];

$RingFVariables = {};

Clear[RingF];

SetAttributes[RingF, HoldAll];
(* this definitino seems to cause drastic slowdown if used in big sums, so disabled *)
(*RingF /: HoldPattern[Plus[r__RingF]] := Plus@@(RingF[Evaluate[Merge[First /@ #, Total]]]& /@ Partition[{r}, UpTo[300]]);*)
RingF /: HoldPattern[Plus[rs:(_RingF..)]] := RingF[Evaluate[Merge[First/@{rs}, Total]]];
RingF /: HoldPattern[Times[x__, RingF[p_Association]]] := RingF[Evaluate[x p]]  /; FreeQ[Hold[x], Alternatives[Sequence @@ $RingFVariables, RingF]];

(*RingF /: HoldPattern[ RingF[Association[p1_Rule]]*RingF[Association[p2_Rule]]] := RingF[Evaluate@Association[tAs[p1, p2]]] ;*)

(*RingF /: HoldPattern[ RingF[Association[rules1__]]* RingF[Association[rules2__]]] := Distribute[f$$[{rules1}, {rules2}], List, f$$, Plus, RingF@*Association@*tAs];*)

RingF /: HoldPattern[r1_RingF * r2_RingF] := toRingF[fromRingF[r1] fromRingF[r2]];

RingF/:HoldPattern[D[RingF[as_], s_/;MemberQ[$RingFVariables, s]]] :=  Scope[
	vpos = PositionFlat[$RingFVariables, s]//First;
	f[sp_SparseArray, coeff_] := Let[
		pow = sp[[vpos]],
		If[pow==0, Return[Nothing]];
		SparseArray[MapAt[#-1& ,sp, vpos]] -> pow coeff
	];
	
	RingF[KeyValueMap[f, as]//Association//Evaluate]
];

RingF/:HoldPattern[D[r_RingF, s_/;!MemberQ[$RingFVariables, s]]] :=  MapOverRingF[D[#,s]&][r];

RingF[Evaluate[Association[]]] = 0;

SetAttributes[tAs, HoldAll];
(* need to go through Normal representation basically to sort the values, otherwise they can appeat unsorted *)
tAs[p1_Rule, p2_Rule] := SparseArray[Normal[p1[[1]] + p2[[1]]]] -> p1[[2]]*p2[[2]] // ReleaseHold;

Clear[toRingF, fromRingF];
toRingF[expr_, ringVars_List/;Length[ringVars]==0] := RingF[Evaluate[Association[{SparseArray[{1->1}] -> expr}]]];
toRingF[expr_, ringVars_List] := RingF[Evaluate[ Association[ MapAt[SparseArray, CoefficientRules[expr, ringVars], {All, 1}]]]];

toRingF[expr_] := toRingF[expr,$RingFVariables];

toRingF[0] = 0;
toRingF[0,vars_List] = 0;

fromRingF[expr_] := expr /. RingF[a1_Association] :> FromCoefficientRules[Normal[KeyMap[Normal, a1]], $RingFVariables];
fromRingF[expr_, ringVars_List] := expr /. RingF[a1_Association] :> FromCoefficientRules[Normal[KeyMap[Normal, a1]], ringVars];

MapOverRingF[f_][r_RingF]:= MapAt[f, r, {1,All}];
MapOverRingFParallel[f_][RingF[a_Association]]:= RingF[Evaluate[ParallelMap[f, a]]];

MapOverRingF[f_][0]:= 0;
MapOverRingFParallel[f_][0]:= 0;

dropZeroes[RingF[a_Association]] := RingF[Evaluate[DeleteCases[a,0]]];
dropZeroes[0] := 0;

getMonomial[variables_List][l_List] := Times @@ ((Power[variables[[First[#1]]], #2] &) @@@ l);

toMonomialList[r_RingF, variables_] := If[variables==={},
    {1 -> r[[1,1]]},
    KeyMap[getMonomial[variables]@*Most@*ArrayRules, First[r]]
];

toMonomialList[r_RingF] := toMonomialList[r,$RingFVariables];

toMonomialList[0, variables_] := 0;
toMonomialList[0] := 0;


cancelFactor::nof = "Cannot cancel factor `1` from RingF \n`2`\n because it's not a factor.";

cancelFactor[RingF[as_Association], v_/;MemberQ[$RingFVariables, v]] := ScopeEnclose[
	vpos = PositionFlat[$RingFVariables,v]//First;
	
	f[sp_SparseArray, coeff_] := Let[
		pow = ConfirmBy[sp[[vpos]], #>0&, Message[cancelFactor::nof,v,toMonomialList[RingF[as]]]];
		SparseArray[MapAt[#-1&,sp, vpos]] -> coeff
	];
	
	RingF[KeyValueMap[f,as]//Association//Evaluate]
];

cancelFactor[0, v_/;MemberQ[$RingFVariables, v]] := 0;

addTerminationPattern[cancelFactor];

End[];

EndPackage[];
